#include<iostream>
using namespace std;
class NeoStudent {       // The class
  public:             // Access specifier
    int stdId;        // Attribute (int variable)
    string stdName;  // Attribute (string variable)
};

int main() {
  NeoStudent stdObj;  // Create an object of MyClass

  // Access attributes and set values
  stdObj.stdId=101;
  stdObj.stdName="Rahul";

  // Print attribute values
  cout << stdObj.stdId <<"\n";
  cout << stdObj.stdName;

  return 0;
}
